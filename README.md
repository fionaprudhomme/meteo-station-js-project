# Projet Station Météo  



Il s'agit de réaliser la partie Software d'une station météo composé de sondes et d'une centrale. Chaque sonde est relié à plusieurs capteurs qui donnent des informations diverses :

- Température
- Hygrométrie
- Pression atmosphérique
- Pluviométrie
- Luminosité
- Vitesse et direction du vent 
- Position GPS et heure
- ... 

Une centrale météo peut être abonnée à plusieurs sondes et présente les données de ces dernières.



## Sonde

Service web réalisé en node.js sur une raspberry permettant la :

- Récupération de la dernière valeur d'une donnée d'un capteur ou de l'ensemble des capteurs.
- Récupération d'un échantillon de données (un ou plusieurs capteurs) sur une période donnée

Travaille inspiré de : http://developers.pioupiou.fr/

Pour activer le server : 

- lancer la commande : sh bdd.sh 
- aller dans le dossier Server
- lancer npm install
- lancer node server.js ou bien forever start server.js (pour avoir le server en continue)



## Centrale

Site web sous forme de Dashboard, il récupère toutes les données de chaque sonde en temps réel. Cette application est responsive et accessible, utilisant Vue-cli.

Sur ce site comprend :

- Un **Dashboard** prédéfini présentant toutes les dernières données disponible pour une sonde :
- L'historique sur 1semaine/1mois/1an des données d'une sonde
- Un comparatif d'une donnée sur toutes les sondes (instantanée ou en mode graphique sur une durée définie)
- Une cartographie représentant les sondes (Utilisez la configuration  des générateurs factices pour changer la position approchée des sondes)

Pour accéder au dashboard (vous avez un readme dans le dossier meteo_dashboard) :

- lancer les commandes :
  - npm install
  - npm run serve