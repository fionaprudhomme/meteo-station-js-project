let loadDataButton = document.getElementById('loadData');
let liveRadio = document.getElementById('live');
let archiveRadio = document.getElementById('archive');
let typeRadio = document.getElementsByName('typeReq');
let capteursCheckbox = document.getElementsByName('capteurs');
let startAndStop = document.getElementById('startAndStop');
let allCheckBox = document.getElementById('all');
let raspberryRadio = document.getElementsByName('typeRas');
let piensgRadio9 = document.getElementById('piensg009');
let piensgRadio10 = document.getElementById('piensg010');
let piensgRadio11 = document.getElementById('piensg011');
let piensgRadio18 = document.getElementById('piensg018');

var map = L.map('map').setView([51.505, -0.09], 5);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


startAndStop.style.display = "none";
for (var i = 0; i < typeRadio.length; i++) {
  typeRadio[i].addEventListener('change', function(event){
    if(archiveRadio.checked){
      startAndStop.style.display = "block";
    }else{
      startAndStop.style.display = "none";
    }
  });
}

for (var i = 0; i < capteursCheckbox.length; i++) {
  capteursCheckbox[i].addEventListener('change', function(event){
    if(this.value != 'all'){
      allCheckBox.checked = false;
    }
  });
}

allCheckBox.addEventListener('change', function(event){
  for (var i = 0; i < capteursCheckbox.length; i++) {
    if(capteursCheckbox[i].value != 'all' && capteursCheckbox[i].checked){
      capteursCheckbox[i].checked = false;
    }
  }
})

loadDataButton.addEventListener('click', function(event){
  isLive = liveRadio.checked;
  isArchive = archiveRadio.checked;
  isPiensg = piensgRadio9.checked;

  let str = "";
  if(piensgRadio9.checked){
    str = "http://piensg009.ensg.eu";
  }
  else if(piensgRadio10.checked){
    str = "http://piensg010.ensg.eu";
  }
  else if(piensgRadio11.checked){
    str = "http://piensg011.ensg.eu";
  }
  else{
    str = "";
  }

console.log(isPiensg);
console.log(str);


  let name=document.getElementsByName('capteurs');
  if(isLive) str+= "/live?capteurs=";
  else str += "/archive?capteurs="
  console.log(str);
  for(i=0;i<(name.length);i++){
    if(name[i].checked){
      str+=name[i].value+",";
    }
  }

  if(str.length>0){str=str.substring(0,str.length-1)};// remove the last comma
  if(isArchive){
      let start = document.getElementById('start');
      let stop = document.getElementById('stop');
      str += "&start="+start.value+"&stop="+stop.value;
  }


  (async () => {

    const result = await fetch(str)
    const json = await result.json()

    console.log(json);
    if(json.result.coordinate.longitude){
      var marker = L.marker([json.result.coordinate.longitude,json.result.coordinate.longitude]).addTo(map);
    }

    document.getElementById('lv').innerHTML = "";
    document.getElementById('arc').innerHTML = "";
    if(json.error){
      document.getElementById('metadata').innerText = 'Une erreur est survenue, verifiez les paramètres'
      return;
    }
    document.getElementById('metadata').innerText ='Sonde : ' + json.result.metadata.nom;
    if(isLive){
      if(json.result.measurements.temperature){
        document.getElementById('lv').innerHTML += ' <h3> ' + json.result.measurements.temperature.description[0] + '</h3>'
        document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.temperature.units[0]+'</strong> : '+ json.result.measurements.temperature.data[0][0] +'</span><br>'
        document.getElementById('lv').innerHTML += '<span><strong>°'+json.result.measurements.temperature.units[1]+'</strong> : '+ json.result.measurements.temperature.data[0][1] +'</span>'
      }
      if(json.result.measurements.pressure){
        document.getElementById('lv').innerHTML += ' <h3> ' + json.result.measurements.pressure.description[0] + '</h3>'
        document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.pressure.units[0]+'</strong> : '+ json.result.measurements.pressure.data[0][0] +'</span><br>'
        document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.pressure.units[1]+'</strong> : '+ json.result.measurements.pressure.data[0][1] +'</span>'
      }
      if(json.result.measurements.humidity){
        document.getElementById('lv').innerHTML += ' <h3> ' + json.result.measurements.humidity.description[0] + '</h3>'
        document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.humidity.units[0]+'</strong> : '+ json.result.measurements.humidity.data[0][0] +'</span><br>'
        document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.humidity.units[1]+'</strong> : '+ json.result.measurements.humidity.data[0][1] +'</span>'
      }
      if(json.result.measurements.luminosity){
        document.getElementById('lv').innerHTML += ' <h3> ' + json.result.measurements.luminosity.description[0] + '</h3>'
        document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.luminosity.units[0]+'</strong> : '+ json.result.measurements.luminosity.data[0][0] +'</span><br>'
        document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.luminosity.units[1]+'</strong> : '+ json.result.measurements.luminosity.data[0][1] +'</span>'
      }
      if(json.result.measurements.wind){
        let i;
        for(i = 0 ; i+1 <= json.result.measurements.wind.description.length ; i ++ ){
          document.getElementById('lv').innerHTML += ' <h3> ' + json.result.measurements.wind.description[i] + '</h3>'
          document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.wind.units[0]+'</strong> : '+ json.result.measurements.wind.data[0][0] +'</span><br>'
          document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.wind.units[i+1]+'</strong> : '+ json.result.measurements.wind.data[0][i+1] +'</span><br>'
        }
      }
      if(json.result.measurements.rain){

        document.getElementById('lv').innerHTML += ' <h3> Dernière date de basculement (Pluie) </h3>'
        document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.rain.units+'</strong> : '+ json.result.measurements.rain.data[0] +'</span><br>'

      }
    }
    else if(isArchive){
      let labels = [];
      let data = [];
      let i;
      if(json.result.measurements.temperature){
        document.getElementById('arc').innerHTML += '<h1> Temperature </h1>';
        document.getElementById('arc').innerHTML += `<div><canvas id="temp" width="200" height="200"></canvas><div>`
        let ctx = document.getElementById("temp").getContext('2d');
        for(i = 0 ; i < json.result.measurements.temperature.length ; i++){
          labels[i] = json.result.measurements.temperature.data[i][0];
          data[i] = json.result.measurements.temperature.data[i][1];
        }
        let myChart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: labels,
            datasets: [{
              label: json.result.measurements.temperature.units[1],
              data: data,
              backgroundColor : 'rgba(255, 0, 0, 1)',
            }]
          },
          options: {
            responsive: true,
            maintainAspectRatio:false,
              title: {
                display: true,
                text: 'Temperature'
              },
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }],
                  xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 90,
                        minRotation: 90
                    }
                  }]
              }
            }
        });
      }
      if(json.result.measurements.pressure){
        document.getElementById('arc').innerHTML += `<div><canvas id="press" width="200" height="200"></canvas></div>`
        let ctx2 = document.getElementById("press").getContext('2d');
        for(i = 0 ; i < json.result.measurements.pressure.length ; i++){
          labels[i] = json.result.measurements.pressure.data[i][0];
          data[i] = json.result.measurements.pressure.data[i][1];
        }
        let myChart2 = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: json.result.measurements.pressure.units[1],
                    data: data,
                    backgroundColor : 'rgba(255, 125, 32, 1)',
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio:false,
                title: {
                  display: true,
                  text: 'Pression'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                      ticks: {
                          autoSkip: false,
                          maxRotation: 90,
                          minRotation: 90
                      }
                  }]
                }
              }
            });
      }
      if(json.result.measurements.humidity){
        document.getElementById('arc').innerHTML += '<h1> Humidité </h1>';
        document.getElementById('arc').innerHTML += `<div><canvas id="hum" width="200" height="200"></canvas></div>`
        let ctx3 = document.getElementById("hum").getContext('2d');
        for(i = 0 ; i < json.result.measurements.humidity.length ; i++){
          labels[i] = json.result.measurements.humidity.data[i][0];
          data[i] = json.result.measurements.humidity.data[i][1];
        }
        let myChart3 = new Chart(ctx3, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: json.result.measurements.humidity.units[1],
                    data: data,
                    backgroundColor : 'rgba(125, 125, 255, 1)',
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio:false,
                title: {
                  display: true,
                  text: 'Humidité'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                      ticks: {
                          autoSkip: false,
                          maxRotation: 90,
                          minRotation: 90
                      }
                    }]
                }
              }
        });
      }
      if(json.result.measurements.luminosity){
        document.getElementById('arc').innerHTML += `<div><canvas id="lum" width="200" height="200"></canvas></div>`
        let ctx4 = document.getElementById("lum").getContext('2d');
        for(i = 0 ; i < json.result.measurements.luminosity.length ; i++){
          labels[i] = json.result.measurements.luminosity.data[i][0];
          data[i] = json.result.measurements.luminosity.data[i][1];
        }
        let myChart4 = new Chart(ctx3, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: json.result.result.measurements.luminosity.units[1],
                    data: data,
                    backgroundColor : 'rgba(125, 125, 255, 1)',
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio:false,
                title: {
                  display: true,
                  text: 'Luminosité'
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                      ticks: {
                          autoSkip: false,
                          maxRotation: 90,
                          minRotation: 90
                      }
                    }]
                }
              }
        });
      }
      if(json.result.measurements.wind){
        document.getElementById('arc').innerHTML += '<h1> Vent </h1>';
        let i, j;
        for(i = 0 ; i+1 <= json.result.measurements.wind.description.length ; i ++ ){
          document.getElementById('lv').innerHTML += ' <h3> ' + json.result.measurements.wind.description[i] + '</h3>'
          for(j = 0 ; j < json.result.measurements.wind.data.length ; j++){
            document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.wind.units[0]+'</strong> : '+ json.result.measurements.wind.data[j][0] +'</span><br>'
            document.getElementById('lv').innerHTML += '<span><strong>'+json.result.measurements.wind.units[i+1]+'</strong> : '+ json.result.measurements.wind.data[j][i+1] +'</span><br>'
          }
        }
      }

    }
  })()
});
