const express = require('express');
// import socketIO from "socket.io";
const fs = require('fs');
const Influx = require('influx');
const fetch = require("node-fetch");
var GPS = require('gps');

const app = express()
const port = 8082

const path = require('path');
const router = express.Router();

app.use(express.static(__dirname));
app.use(express.json());
////////////////////////////////////Globals/////////////////////////////////////
let gpggaJson;
let gprmcJson;
let str;

////////////////////////////////////Files///////////////////////////////////////
//let sensorsFilePath = path.join(__dirname+'/shm/sensors');
//let rainCounterFilePath = path.join(__dirname+'/shm/rainCounter.log');
//let gpsNmeaFilePath = path.join(__dirname+'/shm/gpsNmea');
let sensorsFilePath = '/dev/shm/sensors';
let gpsNmeaFilePath = '/dev/shm/gpsNmea';
let rainCounterFilePath = '/dev/shm/rainCounter.log';
///////////////////////////////////Read Files //////////////////////////////////
function readSensors(){
  let sensors = fs.readFileSync(sensorsFilePath);
  let sensorsJson = JSON.parse(sensors);
  return sensorsJson;
}

function readRainCounter(){
  let rain = fs.readFileSync(rainCounterFilePath, 'utf-8');
  return rain.toString();
}


function readGps(){
  let gpsFile = fs.readFileSync(gpsNmeaFilePath, 'utf-8');

  let gps = new GPS;

  let sentence = gpsFile.split('\n')[0];
  let sentence2 =  gpsFile.split('\n')[1];

  gps.on('data', function(parsed) {
    if(parsed.type == 'GGA'){
      gpggaJson = parsed;
    }
    else if (parsed.type == 'RMC'){
      gprmcJson = parsed;
    }
  });
  gps.update(sentence);
  gps.update(sentence2);
}

/////////////////////////////////////////////Data Base connection//////////////////////////////////////////////


//const influx = new Influx.InfluxDB('http://admin:admin@172.31.58.24:8086/stationbdd');
const influx = new Influx.InfluxDB('http://admin:admin@localhost:8086/stationbdd');

influx.getDatabaseNames()
  .then(names => {
      if (names.includes('stationbdd')) {
      console.log("Connection established !! ");
    }
  }).catch(err => {
  console.error(err);
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Data Base update/////////////////////////////////////////////////////

fs.watchFile(sensorsFilePath, (curr, prev) => {
  let sensorsJson = readSensors();
  influx.writePoints([
  {
    measurement: 'temperature',
    tags : {
      desc : sensorsJson.measure[0].desc,
      unit : sensorsJson.measure[0].unit,
    },
    fields : {
      data : sensorsJson.measure[0].value
    },
    timestamp: (new Date(sensorsJson.date))
  }
  ], {
  database: 'stationbdd',
  });

  influx.writePoints([
  {
    measurement: 'pressure',
    tags : {
      desc : sensorsJson.measure[1].desc,
      unit : sensorsJson.measure[1].unit,
    },
    fields : {
      data : sensorsJson.measure[1].value
    },
    timestamp: (new Date(sensorsJson.date))
  }
  ], {
  database: 'stationbdd',
  });
  influx.writePoints([
  {
    measurement: 'humidity',
    tags : {
      desc : sensorsJson.measure[2].desc,
      unit : sensorsJson.measure[2].unit,
    },
    fields : {
      data : sensorsJson.measure[2].value
    },
    timestamp: (new Date(sensorsJson.date))
  }
  ], {
  database: 'stationbdd',
  });
  influx.writePoints([
  {
    measurement: 'luminosity',
    tags : {
      desc : sensorsJson.measure[3].desc,
      unit : sensorsJson.measure[3].unit,
    },
    fields : {
      data : sensorsJson.measure[3].value
    },
    timestamp: (new Date(sensorsJson.date))
  }
  ], {
  database: 'stationbdd',
  });
  influx.writePoints([
  {
    measurement: 'wind',
    tags : {
      desc : sensorsJson.measure[4].desc+','+sensorsJson.measure[5].desc+','+sensorsJson.measure[6].desc+','+sensorsJson.measure[7].desc,
      unit : sensorsJson.measure[4].unit+','+sensorsJson.measure[5].unit+','+sensorsJson.measure[6].unit+','+sensorsJson.measure[7].unit,
    },
    fields : {
      data : sensorsJson.measure[4].value+','+sensorsJson.measure[5].value+','+sensorsJson.measure[6].value+','+sensorsJson.measure[7].value
    },
    timestamp: (new Date(sensorsJson.date))
  }
  ], {
  database: 'stationbdd',
  });

});

fs.watchFile(gpsNmeaFilePath, (curr, prev) => {
  readGps();
  influx.writePoints([
    {
      measurement: 'GPS_GGA',
      tags : {
        alt : gpggaJson.alt,
        quality : gpggaJson.quality,
        satellites : gpggaJson.satellites,
        hdop : gpggaJson.hdop,
        geoidal : gpggaJson.geoidal,
        age : gpggaJson.age,
        stationID : gpggaJson.stationID,
        raw : gpggaJson.raw,
        valid : gpggaJson.valid,
        type : gpggaJson.type
      },
      fields : {
        lat : gpggaJson.lat,
        lon : gpggaJson.lon,
      },
      timestamp: (new Date(gpggaJson.time))
    }
    ], {
    database: 'stationbdd',
    });
    influx.writePoints([
      {
        measurement: 'GPS_RMC',
        tags : {
          status : gprmcJson.status,
          speed : gprmcJson.speed,
          track : gprmcJson.track,
          variation : gprmcJson.variation,
          faa : gprmcJson.faa,
          navStatus : gprmcJson.navStatus,
          raw : gprmcJson.raw,
          valid : gprmcJson.valid,
          type : gprmcJson.type
        },
        fields : {
          lat : gprmcJson.lat,
          lon : gprmcJson.lon,
        },
        timestamp: (new Date(gprmcJson.time))
      }
      ], {
      database: 'stationbdd',
      });
});

fs.watchFile(rainCounterFilePath, (curr, prev) => {
  let data = readRainCounter();
  console.log(data);
  influx.writePoints([
  {
   measurement: 'rain',
   tags : {
     desc : "Dates des basculements",
     unit : "date",
   },
   fields : {
     data : data.replace("\n","")
   },
   timestamp: new Date(data.replace("\n",""))
  }
  ], {
  database: 'stationbdd',
  });
  });


  influx.getMeasurements()
    .then(names => console.log('My measurement names are: ' + names.join(', ')))
    .catch(error => console.log({ error }));

  async function getTemp(start,stop, isLive){
    if(isLive){
      str = 'select * from temperature ORDER BY time DESC LIMIT 1';
    }else{
      str = 'select * from temperature where time <= ' +"'"+new Date(stop).toISOString() +"'"+ ' and time >= '+"'"+ new Date(start).toISOString() +"'";
    }
    const p = await influx.query(str).then(resu => {
    for(let i=0;i<resu.length;i++){
      resu[i]['datat'] = [];
      resu[i]['units'] = resu[i]['unit'].split(',');
      resu[i]['description'] = resu[i]['desc'].split(',');
      resu[i]['datat'][i] = resu[i]['data'].split(',');
      resu[i]['units'].unshift("date");
      resu[i]['datat'][i].unshift(resu[i].time);

      delete resu[i]['time'];
      delete resu[i]['desc'];
      delete resu[i]['unit'];
      resu[i]['data'] = []
      resu[i]['data'][i] = resu[i]['datat'][i]
      delete resu[i]['datat'];

    }

    return(resu);

  });
  return p;
};

async function getHum(start,stop, isLive){

  if(isLive){
    str = 'select * from humidity ORDER BY time DESC LIMIT 1';
  }else{
    str = 'select * from humidity where time <= ' +"'"+new Date(stop).toISOString() +"'"+ ' and time >= '+"'"+ new Date(start).toISOString() +"'";
  }
  const p = await influx.query(str).then(resu => {
    for(let i=0;i<resu.length;i++){
      resu[i]['datat'] = [];
      resu[i]['units'] = resu[i]['unit'].split(',');
      resu[i]['description'] = resu[i]['desc'].split(',');
      resu[i]['datat'][i] = resu[i]['data'].split(',');
      resu[i]['units'].unshift("date");
      resu[i]['datat'][i].unshift(resu[i].time);

      delete resu[i]['desc'];
      delete resu[i]['time'];
      delete resu[i]['unit'];
      resu[i]['data'] = []
      resu[i]['data'][i] = resu[i]['datat'][i]
      delete resu[i]['datat'];

    }

    return(resu);

  });

  return p;
};

async function getLum(start,stop, isLive){
  if(isLive){
    str = 'select * from luminosity ORDER BY time DESC LIMIT 1';
  }else{
    str = 'select * from luminosity where time <= ' +"'"+new Date(stop).toISOString() +"'"+ ' and time >= '+"'"+ new Date(start).toISOString() +"'";
  }
  const p = await influx.query(str).then(resu => {
    for(let i=0;i<resu.length;i++){
      resu[i]['datat'] = [];
      resu[i]['units'] = resu[i]['unit'].split(',');
      resu[i]['description'] = resu[i]['desc'].split(',');
      resu[i]['datat'][i] = resu[i]['data'].split(',');
      resu[i]['units'].unshift("date");
      resu[i]['datat'][i].unshift(resu[i].time);

      delete resu[i]['time'];
      delete resu[i]['desc'];
      delete resu[i]['unit'];
      resu[i]['data'] = []
      resu[i]['data'][i] = resu[i]['datat'][i]
      delete resu[i]['datat'];

    }

    return(resu);
  });

  return p;
};

async function getPre(start,stop, isLive){
  if(isLive){
    str = 'select * from pressure ORDER BY time DESC LIMIT 1';
  }else{
    str = 'select * from pressure where time <= ' +"'"+new Date(stop).toISOString() +"'"+ ' and time >= '+"'"+ new Date(start).toISOString() +"'";
  }
  const p = await influx.query(str).then(resu => {
    for(let i=0;i<resu.length;i++){
      resu[i]['datat'] = [];
      resu[i]['units'] = resu[i]['unit'].split(',');
      resu[i]['description'] = resu[i]['desc'].split(',');
      resu[i]['datat'][i] = resu[i]['data'].split(',');
      resu[i]['units'].unshift("date");
      resu[i]['datat'][i].unshift(resu[i].time);

      delete resu[i]['time'];
      delete resu[i]['desc'];
      delete resu[i]['unit'];
      resu[i]['data'] = []
      resu[i]['data'][i] = resu[i]['datat'][i]
      delete resu[i]['datat'];
    }

    return(resu);

  });

  return p;
};

async function getWin(start,stop, isLive){
  if(isLive){
    str = 'select * from wind ORDER BY time DESC LIMIT 1';
  }else{
    str = 'select * from wind where time <= ' +"'"+new Date(stop).toISOString() +"'"+ ' and time >= '+"'"+ new Date(start).toISOString() +"'";
  }
  const p = await influx.query(str).then(resu => {
    for(let i=0;i<resu.length;i++){
      resu[i]['datat'] = [];
      resu[i]['units'] = resu[i]['unit'].split(',');
      resu[i]['description'] = resu[i]['desc'].split(',');
      resu[i]['datat'][i] = resu[i]['data'].split(',');
      resu[i]['units'].unshift("date");
      resu[i]['datat'][i].unshift(new Date(resu[i].time));

      delete resu[i]['time'];
      delete resu[i]['desc'];
      delete resu[i]['unit'];
      resu[i]['data'] = []
      resu[i]['data'][i] = resu[i]['datat'][i]
      delete resu[i]['datat'];

    }

    return(resu);

  });

  return p;
};

async function getRain(start,stop, isLive){
  if(isLive){
    str = 'select * from rain ORDER BY time DESC LIMIT 1';
  }else{
    str = 'select * from rain where time <= ' +"'"+new Date(stop).toISOString() +"'"+ ' and time >= '+"'"+ new Date(start).toISOString() +"'";
    //str = 'select * from rain';
  }
  const p = await influx.query(str).then(resu => {
    for(let i=0;i<resu.length;i++){
      resu[i]['units'] = [];
      resu[i]['datat'] = [];
      resu[i]['datat'][i] = resu[i]['data']
      resu[i]['units'][i] = resu[i]['unit']
      resu[i]['description'] = resu[i]['desc']
      delete resu[i]['time'];
      delete resu[i]['desc'];
      delete resu[i]['unit'];
      resu[i]['data'] = []
      resu[i]['data'][i] = resu[i]['datat'][i]
      delete resu[i]['datat'];

    }

    return resu;


  });
  return p;
}

async function getCoords(start,stop, isLive){
  if(isLive){
    str = 'select * from GPS_GGA ORDER BY time DESC LIMIT 1';
  }else{
    //str = 'select * from rain where time <= ' +"'"+new Date(stop).toISOString() +"'"+ ' and time >= '+"'"+ new Date(start).toISOString() +"'";
    str = 'select * from GPS_GGA ORDER BY time DESC LIMIT 1';
  }
  let toReturn = {};
  const p = await influx.query(str).then(resu => {
    for(let i=0;i<resu.length;i++){
      toReturn['longitude'] = resu[i]['lon'];
      toReturn['latitude'] = resu[i]['lat'];
      toReturn['date'] = resu[i]['time'];
      toReturn['success'] = true;
    }

    return toReturn;

  });
  return p;
}

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send({error : "Something broke!"})
});

router.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/index.html'));
});

router.get('/archive', (req, res) => {
  if(req.query.capteurs == null || req.query.start == null || req.query.stop == null){
    res.json({error : "t'as fais de la merde"})
  }
  (async () => {
    let captors = req.query.capteurs.split(',')

    let m = {}
    m['result'] = {}
    m['result']['id'] = 18
    m['result']['metadata'] = {'nom' : 'Piensg 018'}
    m['result']['coordinates'] = await getCoords(req.query.start, req.query.stop, false)
    m['result']['measurements'] = {}
    for(let i = 0; i < captors.length; i++){
      if(captors[i] == 'tem'){
        m['result']['measurements']['temperature'] = await getTemp(req.query.start, req.query.stop, false)
      }
      if(captors[i] == 'pre'){
        m['result']['measurements']['pressure'] = await getPre(req.query.start, req.query.stop, false)
      }
      if(captors[i] == 'hum'){
        m['result']['measurements']['humidity'] = await getHum(req.query.start, req.query.stop, false)
      }
      if(captors[i] == 'lum'){
        m['result']['measurements']['luminosity'] = await getLum(req.query.start, req.query.stop, false)
      }
      if(captors[i] == 'win'){
        m['result']['measurements']['wind'] = await getWin(req.query.start, req.query.stop, false)
      }
      if(captors[i] == 'ran'){
        m['result']['measurements']['rain'] = await getRain(req.query.start, req.query.stop, false)
      }
      if(captors[i] == 'all'){
        m['result']['measurements']['temperature'] = await getTemp(req.query.start, req.query.stop, false)
        m['result']['measurements']['pressure'] = await getPre(req.query.start, req.query.stop, false)
        m['result']['measurements']['humidity'] = await getHum(req.query.start, req.query.stop, false)
        m['result']['measurements']['luminosity'] = await getLum(req.query.start, req.query.stop, false)
        m['result']['measurements']['wind'] = await getWin(req.query.start, req.query.stop, false)
        m['result']['measurements']['rain'] = await getRain(req.query.start, req.query.stop, false)
      }
    }
    res.json(m)
  })()
});

router.get('/live', (req, res) => {
  if(req.query.capteurs == null){
    res.json({error : "t'as fais de la merde"})
  }
  (async () => {
    let captors = req.query.capteurs.split(',')

    let m = {}
    m['result'] = {}
    m['result']['id'] = 18
    m['result']['metadata'] = {'nom' : 'Piensg 018'}
    m['result']['coordinates'] = await getCoords(null, null, true)
    m['result']['measurements'] = {}
    for(let i = 0; i < captors.length; i++){
      if(captors[i] == 'tem'){
        m['result']['measurements']['temperature'] = await getTemp(null, null, true)
      }
      if(captors[i] == 'pre'){
        m['result']['measurements']['pressure'] = await getPre(null, null, true)
      }
      if(captors[i] == 'hum'){
        m['result']['measurements']['humidity'] = await getHum(null, null, true)
      }
      if(captors[i] == 'lum'){
        m['result']['measurements']['luminosity'] = await getLum(null, null, true)
      }
      if(captors[i] == 'win'){
        m['result']['measurements']['wind'] = await getWin(null, null, true)
      }
      if(captors[i] == 'ran'){
        m['result']['measurements']['rain'] = await getRain(null, null, true)
      }
      if(captors[i] == 'all'){
        m['result']['measurements']['temperature'] = await getTemp(null, null, true)
        m['result']['measurements']['pressure'] = await getPre(null, null, true)
        m['result']['measurements']['humidity'] = await getHum(null, null, true)
        m['result']['measurements']['luminosity'] = await getLum(null, null, true)
        m['result']['measurements']['wind'] = await getWin(null, null, true)
        m['result']['measurements']['rain'] = await getRain(null, null, true)
      }
    }
    res.json(m)
  })()
});

//add the router
app.use('/', router);
app.listen(process.env.port || port);

console.log('Running at Port '+port);
